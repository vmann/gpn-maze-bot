[ -f Build/build.ninja ] || cmake -G Ninja -B Build -DCMAKE_BUILD_TYPE=Release
exec ninja -C Build "$@"
